from fastapi import FastAPI, File, UploadFile
from models import PCBSpecification, TestPoint
from mappings import TestPointSideMapping, TestPointTypeMapping, TestPointMapping
from utility import get_pcb_specifications, get_test_point
import pandas

app = FastAPI()


@app.post("/fixturfab")
async def fixturfab(file: UploadFile = File(None)):
    contents = await file.read()
    specs_sheet = pandas.read_excel(contents, sheet_name=1)
    test_points_sheet = pandas.read_excel(contents, sheet_name=2)
    pcb_specs = PCBSpecification(**get_pcb_specifications(specs_sheet.to_dict()))
    test_points = []
    headers = [key for key, _ in TestPointMapping.items()]
    for row in range(len(test_points_sheet[headers[0]])):
        data = {}
        for header_name in headers:
            data[header_name] = test_points_sheet[header_name][row]
        test_points.append(TestPoint(**get_test_point(data)))
    return {
        # optional return, this can be included in the documentation instead
        "legend": {
            "test_point_list__side": {value: key for key, value in TestPointSideMapping.items()},
            "test_point_list__type": {value: key for key, value in TestPointTypeMapping.items()}
        },
        "pcb_specification": pcb_specs.dict(),
        "test_point_list": test_points
    }
