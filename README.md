# FixturFab DUT Information Parser

## Endpoints

### POST /fixturfab

#### Payload
1. `file` - Contains the `.xlsx` template file of DUT Information

#### Return
1. `legend` - the equivalent values of numbers to literal strings for some fields in `test_point_list` array
2. `pcb_specification` - the specifications of PCB
3. `test_point_list` - the test points