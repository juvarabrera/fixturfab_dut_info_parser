from typing import Union, Dict
from mappings import PcbSpecificationMapping, TestPointMapping, \
     TestPointSideMapping, TestPointTypeMapping


def parse_mm(mm_value: Union[str, float]):
    if isinstance(mm_value, str):
        return float(mm_value.replace("mm", ""))
    return None


def parse_string(str_val: Union[str, float]):
    if isinstance(str_val, str):
        return str_val
    return ""


def get_pcb_specifications(pcb_specs: Dict):
    specs = {}
    for key, value in pcb_specs['PCB Information'].items():
        specs[PcbSpecificationMapping[value.strip()]] = pcb_specs['Value'][key]
    return specs


def get_test_point(test_point_data: Dict):
    test_point = {}
    for key, value in test_point_data.items():
        if key in ['Net', 'Name']:
            value = parse_string(value)
        elif key in ['X Coord', 'Y Coord', 'Hole Size']:
            value = parse_mm(value)
        elif key == 'Side':
            value = TestPointSideMapping[value]
        elif key == 'Type':
            value = TestPointTypeMapping[value]
        test_point[TestPointMapping[key]] = value
    return test_point
