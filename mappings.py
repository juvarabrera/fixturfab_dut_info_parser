from models import TestPointSide, TestPointType

PcbSpecificationMapping = {
    'Height (mm)': 'height_mm',
    'Width (mm)': 'width_mm',
    'Thickness (mm)': 'thickness_mm'
}

TestPointMapping = {
    'Net': 'net',
    'Name': 'name',
    'X Coord': 'x_mm',
    'Y Coord': 'y_mm',
    'Side': 'side',
    'Type': 'type',
    'Hole Size': 'hole_size_mm'
}

TestPointSideMapping = {
    'Top': TestPointSide.TOP,
    'Bottom': TestPointSide.BOTTOM,
    'Both': TestPointSide.BOTH
}

TestPointTypeMapping = {
    'Through Hole': TestPointType.THROUGH_HOLE,
    'SMD': TestPointType.SMD,
    'Locating Pin': TestPointType.LOCATING_PIN,
    'Pressure Pin': TestPointType.PRESSURE_PIN
}
