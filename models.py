from pydantic import BaseModel, Field
from typing import Optional
from enum import Enum


class TestPointSide(Enum):
    TOP = 0
    BOTTOM = 1
    BOTH = 2


class TestPointType(Enum):
    THROUGH_HOLE = 0
    SMD = 1
    LOCATING_PIN = 2
    PRESSURE_PIN = 3


class PCBSpecification(BaseModel):
    height_mm: float = Field(default=None)
    width_mm: float = Field(default=None)
    thickness_mm: float = Field(default=None)


class TestPoint(BaseModel):
    net: Optional[str] = Field(default="")
    name: Optional[str] = Field(default="")
    x_mm: float = Field(default=None)
    y_mm: float = Field(default=None)
    side: TestPointSide
    type: TestPointType
    hole_size_mm: Optional[str] = Field(default=None)
